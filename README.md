## This is a test for NowTv.

### Main reasons why I switched from `create-react-app`
- enzyme was not working worrectly;
- I wanted to use CSS Modules with SASS;

### Folder structure changes:
- I moved some files in their specific directories as it was confusing browsing files.

### Info
- node version used: `9.10`;
- npm version used: `5.6`.

### how to
- `npm start` to run the server;
- `npm run test` to run tests.