import Chat from './containers/chat/main.js';
import './App.scss';

const App = () => <Chat />;

export default App;
