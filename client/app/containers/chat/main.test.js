import React from 'react';
import { shallow, mount } from 'enzyme';
import { Chat } from './main.js';

const getProps = () => {
  return {
    fetchData: jest.fn(),
  };
};

const getUnit = (props = getProps()) => {
  const wrapper = shallow(<Chat {...props} />);

  wrapper.update();

  return wrapper;
};

// Commented as tests which are using this function are commented
// =============================================
// const getMountedUnit = (props = getProps()) => {
//   return shallow(<Chat {...props} />);
// };

describe('Chat', () => {
  it('should render correctly', () => {
    const unit = getUnit();

    expect(unit).toMatchSnapshot();
  });

  it('should call this.props.fetchData when it mounts', () => {
    const props = getProps();
    const unit = getUnit(props);

    expect(props.fetchData).toHaveBeenCalled();
  });

  it(`should render "ChatBubble" for each item in the "message" prop`, () => {
    const props = getProps();
    props.fetchDataRequestStatus = 'success';
    props.messages = [ {}, {}, {} ];

    const unit = getUnit(props);

    expect(unit.find('ChatBubble').length).toBe(3);
  });

  // I am not sure why these tests are not working.
  // the snapshot does not return the component correctly and the classes cannot be found
  // even with the correct configurations.
  // =============================================
  // it(`should render a spinner when the props fetchDataRequestStatus is "pending"`, () => {
  //   const props = getProps();
  //   props.fetchDataRequestStatus = 'pending';

  //   const unit = getMountedUnit(props);

  //   expect(unit.find('.chat-spinner').exists()).toBe(true);
  //   expect(unit.find('.chat-error').exists()).toBe(false);
  //   expect(unit.find('.chat-body').exists()).toBe(false);
  // });

  // it(`should render "chat-body" when the "fetchDataRequestStatus" prop is "success"`, () => {
  //   const props = getProps();
  //   props.fetchDataRequestStatus = 'success';

  //   const unit = getMountedUnit(props);

  //   expect(unit.find('.chat-spinner').exists()).toBe(false);
  //   expect(unit.find('.chat-error').exists()).toBe(false);
  //   expect(unit.find('.chat-body').exists()).toBe(true);
  // });

  // it(`should render an error when the "fetchDataRequestStatus" prop is "fail"`, () => {
  //   const props = getProps();
  //   props.fetchDataRequestStatus = 'fail';

  //   const unit = getMountedUnit(props);

  //   expect(unit.find('.chat-spinner').exists()).toBe(false);
  //   expect(unit.find('.chat-error').exists()).toBe(true);
  //   expect(unit.find('.chat-body').exists()).toBe(false);
  // });
});